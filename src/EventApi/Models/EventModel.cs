using System;
using EventApi.Entities;

namespace EventApi.Models {

    public class EventType {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class EventSubdivision {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Event {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        // public EventType Type { get; set; }
        // public EventSubdivision Subdivision { get; set; }

        // public override string ToString()
        // {
        //     return $"Event {{ Id = {Id}, Name = \"{Name}\" }}";
        // }
    }

    public class EventModel {
        public Event Event { get; set; }
        public EventType Type { get; set; }
        public EventSubdivision Subdivision { get; set; }

        public static EventModel FromCalendarEventDetail(CalendarEventDetail ced)
        {
            return new EventModel {
                Event = new Event {
                    Id = ced.Event.Id,
                    Start = ced.DateTimeLocalFromGE,
                    End = ced.DateTimeLocalToL
                },
                Type = new EventType {
                    Id = ced.Event.Type.Id,
                    Name = ced.Event.Type.Value
                },
                Subdivision = new EventSubdivision {
                    Id = ced.Event.CountrySubdivision.Id,
                    Name = ced.Event.CountrySubdivision.Value
                }
            };
        }
    }

}
