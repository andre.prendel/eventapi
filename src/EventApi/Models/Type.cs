using System.Collections.Generic;

namespace EventApi.Models {

    public class Type {
        public int Id { get; set; }
        public string Value { get; set; }
        ICollection<Event> Event { get; set; }
    }

}
