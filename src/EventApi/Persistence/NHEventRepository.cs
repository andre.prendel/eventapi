using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using EventApi.Entities;


namespace EventApi.Persistence {

    public class NHEventRepository : IEventRepository {

        private ISession _session;

        public NHEventRepository(ISession session)
        {
            _session = session;
        }

        public ICollection<CalendarEventDetail> GetAll()
        {
            CalendarEvent ce = null;
            CalendarEventDetail ced = null;
            KT_CalendarEventType_Internal ct = null;
            KT_CountrySubdivision_Internal cs = null;

            var evs = _session.QueryOver(() => ced)
                .Inner.JoinAlias(() => ced.Event, () => ce)
                .Inner.JoinAlias(() => ce.Type, () => ct)
                .Inner.JoinAlias(() => ce.CountrySubdivision, () => cs)
                .List();

            return evs;
        }

        public CalendarEventDetail GetById(int id)
        {
            var ev = _session.QueryOver<CalendarEventDetail>()
                .List()
                .SingleOrDefault(ev => ev.Id == id);

            return ev;
        }

        public CalendarEventDetail Add(CalendarEventDetail detail)
        {
            using (var transaction = _session.BeginTransaction()) {
                _session.Save(detail);
                transaction.Commit();

                return detail;
            }
        }

        public bool Update(int id, CalendarEventDetail detail)
        {
            var ev = GetById(id);
            if (ev != null) {
                ev.DateTimeLocalFromGE = detail.DateTimeLocalFromGE;
                ev.DateTimeLocalToL = detail.DateTimeLocalToL;
                ev.Event.Type = new KT_CalendarEventType_Internal {
                    Id = detail.Event.Type.Id
                };
                ev.Event.CountrySubdivision = new KT_CountrySubdivision_Internal {
                    Id = detail.Event.CountrySubdivision.Id
                };

                using (var transaction = _session.BeginTransaction()) {
                    _session.SaveOrUpdate(ev);
                    transaction.Commit();
                }
            }

            return ev != null;
        }

        public bool Delete(int id)
        {
            var ev = GetById(id);
            if (ev != null) {

                using (var transaction = _session.BeginTransaction()) {
                    _session.Delete(ev);
                    transaction.Commit();
                }
            }

            return ev != null;
        }

        public ICollection<KT_CalendarEventType_Internal> GetAllTypes()
        {
            var ts = _session.QueryOver<KT_CalendarEventType_Internal>()
                .List();

            return ts;
        }

        public ICollection<KT_CountrySubdivision_Internal> GetAllSubdivisions()
        {
            var cs = _session.QueryOver<KT_CountrySubdivision_Internal>()
                .List();

            return cs;
        }
    }
}
