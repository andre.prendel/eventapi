using System.Collections.Generic;
using EventApi.Entities;

namespace EventApi.Persistence {

    public class MemoryEventRepository : IEventRepository {

        private List<CalendarEventDetail> Events;

        public MemoryEventRepository()
        {
            Events = new List<CalendarEventDetail> {
                new CalendarEventDetail { Id = 1 },
                // new CalendarEventDetail { ID = 2, CountrySubdivision = new KT_CountrySubdivision_Internal {
                //     ID = 2, Value = "bar"
                // } },
                // new CalendarEventDetail { ID = 3, CountrySubdivision = new KT_CountrySubdivision_Internal {
                //     ID = 3, Value = "foobar"
                // }  },
                // new CalendarEventDetail { ID = 4, CountrySubdivision = new KT_CountrySubdivision_Internal {
                //     ID = 4, Value = "foobaz"
                // }  }
            };
        }

        public ICollection<CalendarEventDetail> GetAll()
        {
            return Events;
        }

        public CalendarEventDetail GetById(int id)
        {
            return new CalendarEventDetail {
                Id = 1,
                Event = new CalendarEvent {
                    Id = 1,
                    Type = new KT_CalendarEventType_Internal {
                        Id = 1, Value = "Winterferien"
                    }
                }
            };
        }

        public CalendarEventDetail Add(CalendarEventDetail detail)
        {
            return new CalendarEventDetail {
                Id = 1,
                Event = new CalendarEvent {
                    Id = 1,
                    Type = new KT_CalendarEventType_Internal {
                        Id = 1, Value = "Winterferien"
                    }
                }
            };
        }

        public bool Update(int id, CalendarEventDetail detail)
        {
            return true;
        }

        public bool Delete(int id)
        {
            return true;
        }

        public ICollection<KT_CalendarEventType_Internal> GetAllTypes()
        {
            return new List<KT_CalendarEventType_Internal>();
        }

        public ICollection<KT_CountrySubdivision_Internal> GetAllSubdivisions()
        {
            return new List<KT_CountrySubdivision_Internal>();
        }

    }

}
