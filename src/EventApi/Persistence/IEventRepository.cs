using System.Collections.Generic;
using EventApi.Entities;

namespace EventApi.Persistence {

    public interface IEventRepository {

        ICollection<CalendarEventDetail> GetAll();
        CalendarEventDetail GetById(int id);
        CalendarEventDetail Add(CalendarEventDetail detail);
        bool Update(int id, CalendarEventDetail detail);
        bool Delete(int id);

        ICollection<KT_CalendarEventType_Internal> GetAllTypes();
        ICollection<KT_CountrySubdivision_Internal> GetAllSubdivisions();
    }

}
