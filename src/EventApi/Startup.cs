using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using EventApi.Persistence;

namespace EventApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Console.WriteLine("Server: " + Configuration.GetValue<String>("ConnectionString:Server"));
            Console.WriteLine("Database: " + Configuration.GetValue<String>("ConnectionString:Database"));
            Console.WriteLine("Username: " + Configuration.GetValue<String>("ConnectionString:Username"));
            Console.WriteLine("Password: " + Configuration.GetValue<String>("ConnectionString:Password"));
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                      builder =>
                      {
                          builder.WithOrigins("http://localhost:5000",
                                              "https://localhost:5002",
                                              "http://localhost:5003")
                                              .AllowAnyHeader()
                                              .AllowAnyMethod()
                                              .AllowCredentials();
                      });
            });

            services.AddMvc();

            services.AddControllers();
            services.AddSingleton<ISessionFactory>(provider => {
                ISessionFactory factory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012
                    .ConnectionString(c => c
                        .Server(Configuration.GetValue<String>("ConnectionString:Server"))
                        .Database(Configuration.GetValue<String>("ConnectionString:Database"))
                        .Username(Configuration.GetValue<String>("ConnectionString:Username"))
                        .Password(Configuration.GetValue<String>("ConnectionString:Password")))
                    .ShowSql())
                .Mappings(m =>
                    m.FluentMappings.AddFromAssemblyOf<Program>()
                    .Conventions.Add(ForeignKey.EndsWith("ID")))
                // .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, create))
                .BuildSessionFactory();

                return factory;
            });

            services.AddScoped<ISession>(provider => {
                var factory = provider.GetService<ISessionFactory>();
                return factory.OpenSession();
            });

            services.AddScoped<IEventRepository,NHEventRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
