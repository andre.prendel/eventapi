using System.Collections.Generic;

namespace EventApi.Entities {

    public class CalendarEvent {

        public virtual int Id { get; set; }
        public virtual KT_CalendarEventType_Internal Type { get; set; }
        public virtual KT_CountrySubdivision_Internal CountrySubdivision { get; set; }
        public virtual CalendarEventDetail EventDetail { get; set; }
    }

}
