using System;

namespace EventApi.Entities {

    public class CalendarEventDetail {

        public virtual int Id { get; set; }
        public virtual DateTime DateTimeLocalFromGE { get; set; }
        public virtual DateTime DateTimeLocalToL { get; set; }
        public virtual CalendarEvent Event { get; set; }
    }

}
