using NHibernate;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;

namespace EventApi.Entities {

    public static class SessionFactory {

        public static ISession OpenSession(bool create = false)
        {
            ISessionFactory factory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012
                    .ConnectionString(c => c
                        .Server("localhost")
                        .Database("PTH")
                        .Username("sa")
                        .Password("Yukon900"))
                    .ShowSql())
                .Mappings(m =>
                    m.FluentMappings.AddFromAssemblyOf<Program>()
                    .Conventions.Add(ForeignKey.EndsWith("ID")))
                .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, create))
                .BuildSessionFactory();

                return factory.OpenSession();
        }

    }

}
