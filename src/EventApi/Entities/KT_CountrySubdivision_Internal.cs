namespace EventApi.Entities {

    public class KT_CountrySubdivision_Internal {

        public virtual int Id { get; set; }
        public virtual string Value { get; set; }
    }

}
