using System.Collections.Generic;

namespace EventApi.Entities {

    public class KT_CalendarEventType_Internal {

        public virtual int Id { get; set; }
        public virtual string Value { get; set; }
        // public virtual IList<CalendarEvent> Event { get; set; }
    }

}
