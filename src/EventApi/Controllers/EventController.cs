using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NHibernate.Linq;
using EventApi.Entities;
using EventApi.Models;
using EventApi.Persistence;
using NHibernate;

namespace EventApi.Controllers {

    [ApiController]
    [Route("[controller]")]
    public class EventController : ControllerBase {

        private IEventRepository _repo;

        public EventController(IEventRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<List<EventModel>> Get()
        {
            var evs = _repo.GetAll()
                .Select(e => EventModel.FromCalendarEventDetail(e));

            return Ok(evs);
        }

        [HttpGet("{id}")]
        public ActionResult<EventModel> GetById(int id)
        {
            var e = _repo.GetById(id);

            return Ok(EventModel.FromCalendarEventDetail(e));
        }

        [HttpGet("type/{id}")]
        public ActionResult<List<EventModel>> GetByTypeId(int id)
        {
            var ev = _repo.GetAll()
                .Where(e => e.Event.Type.Id == id)
                .Select(e => EventModel.FromCalendarEventDetail(e));

            return Ok(ev);
        }

        [HttpPost]
        public ActionResult<EventModel> Post(EventModel e)
        {
            var e_ = new CalendarEventDetail {
                DateTimeLocalFromGE = e.Event.Start, DateTimeLocalToL = e.Event.End,
                Event = new CalendarEvent {
                    Type = new KT_CalendarEventType_Internal {
                        Id = e.Type.Id
                    },
                    CountrySubdivision = new KT_CountrySubdivision_Internal {
                        Id = e.Subdivision.Id
                    }
                }
            };
            var ev = _repo.Add(e_);

            return CreatedAtAction(nameof(GetById), new { id = ev.Id }, ev);
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, EventModel ev)
        {
            var ev_ = new CalendarEventDetail {
                DateTimeLocalFromGE = ev.Event.Start,
                DateTimeLocalToL = ev.Event.End,
                Event = new CalendarEvent {
                    Type = new KT_CalendarEventType_Internal {
                        Id = ev.Type.Id
                    },
                    CountrySubdivision = new KT_CountrySubdivision_Internal {
                        Id = ev.Subdivision.Id
                    }
                }
            };

            var ret = _repo.Update(id, ev_);

            if (ret)
                return NoContent();
            else
                return NotFound();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var ret = _repo.Delete(id);

            if (ret)
                return NoContent();
            else
                return NotFound();
        }

        [Route("types")]
        [HttpGet]
        public ActionResult<Models.Type> GetTypes()
        {
            var ts = _repo.GetAllTypes();

            return Ok(ts);
        }

        [Route("subdivisions")]
        [HttpGet]
        public ActionResult<KT_CountrySubdivision_Internal> GetSubdivisions()
        {
            var ts = _repo.GetAllSubdivisions();

            return Ok(ts);
        }
    }
}
