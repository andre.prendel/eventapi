using FluentNHibernate.Mapping;
using EventApi.Entities;

namespace EventApi.Mappings {

    public class CalendarEventMap : ClassMap<CalendarEvent> {

        public CalendarEventMap()
        {
            Id(x => x.Id);
            References(x => x.Type).Column("TypeID");
            References(x => x.CountrySubdivision);
            HasOne(x => x.EventDetail);
        }
    }

}
