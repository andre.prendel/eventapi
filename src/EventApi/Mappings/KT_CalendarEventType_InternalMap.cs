using FluentNHibernate.Mapping;
using EventApi.Entities;

namespace EventApi.Mappings {

    public class KT_CalendarEventType_InternalMap : ClassMap<KT_CalendarEventType_Internal> {

        KT_CalendarEventType_InternalMap()
        {
            Id(x => x.Id);
            Map(x => x.Value);
            // HasMany(x => x.Event).KeyColumn("TypeID").Inverse();
        }

    }

}
