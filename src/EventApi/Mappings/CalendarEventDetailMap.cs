using FluentNHibernate.Mapping;
using EventApi.Entities;

namespace EventApi.Mappings {

    public class CalendarEventDetailMap : ClassMap<CalendarEventDetail> {

        public CalendarEventDetailMap()
        {
            Id(x => x.Id);
            Map(x => x.DateTimeLocalFromGE);
            Map(x => x.DateTimeLocalToL);
            References(x => x.Event, "EventID").Cascade.All();
            // HasOne(x => x.Event);
        }

    }

}
