using FluentNHibernate.Mapping;
using EventApi.Entities;

namespace EventApi.Mappings {

    public class KT_CountrySubdivision_InternalMap : ClassMap<KT_CountrySubdivision_Internal> {

        public KT_CountrySubdivision_InternalMap()
        {
            Id(x => x.Id);
            Map(x => x.Value);
        }
    }

}
