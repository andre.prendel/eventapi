using System;
using System.Collections.Generic;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using EventApi.Controllers;
using EventApi.Models;
using EventApi.Persistence;
using EventApi.Entities;

namespace EventApi.Test {

    public class EventControllerTest {
        [Fact]
        public void EventControllerGet()
        {
            var controller = new EventController(new MemoryEventRepository());
            // var controller = new EventController(new NHEventRepository(SessionFactory.OpenSession()));
            var ret = controller.Get();
            var action = Assert.IsType<ActionResult<List<CalendarEventDetail>>>(ret);
            var result = Assert.IsType<OkObjectResult>(action.Result);
            var value = Assert.IsType<List<CalendarEventDetail>>(result.Value);

            Assert.Equal(1, value.Count);
            // Assert.Contains(value, x => x.Id == 1 &&
            //                        x.DateTimeLocalFromGE == new DateTime(2020, 1, 1) &&
            //                        x.DateTimeLocalToL == new DateTime(2020, 1, 2) &&
            //                        x.Event.Id == 1 &&
            //                        x.Event.Type.Id == 1 &&
            //                        x.Event.Type.Value == "Winterferien" &&
            //                        x.Event.CountrySubdivision.Id == 1 &&
            //                        x.Event.CountrySubdivision.Value == "Schleswig-Holstein");

            // Assert.Contains(value, x => x.Id == 2 &&
            //                        x.DateTimeLocalFromGE == new DateTime(2020, 2, 1) &&
            //                        x.DateTimeLocalToL == new DateTime(2020, 2, 10) &&
            //                        x.Event.Id == 2 &&
            //                        x.Event.Type.Id == 2 &&
            //                        x.Event.Type.Value == "Osterferien" &&
            //                        x.Event.CountrySubdivision.Id == 1 &&
            //                        x.Event.CountrySubdivision.Value == "Schleswig-Holstein");

            // Assert.Contains(value, x => x.ID == 32 && x.CountrySubdivision.ID == 2 && x.CountrySubdivision.Value == "Hamburg");
            // Assert.Contains(value, x => x.ID == 33 && x.CountrySubdivision.ID == 14 && x.CountrySubdivision.Value == "Sachsen");
            // Assert.Contains(value, x => x.ID == 34 && x.CountrySubdivision.ID == 14 && x.CountrySubdivision.Value == "Sachsen");
            // Assert.DoesNotContain(value, x => x.ID == 5);
        }

        [Fact]
        public void EventControllerGetById()
        {
            // var controller = new EventController(new MemoryEventRepository());
            var controller = new EventController(new NHEventRepository(SessionFactory.OpenSession()));
            var ret = controller.GetById(1);
            var action = Assert.IsType<ActionResult<CalendarEventDetail>>(ret);
            var result = Assert.IsType<OkObjectResult>(action.Result);
            var value = Assert.IsType<CalendarEventDetail>(result.Value);
        }

        // [Fact]
        public void EventControllerPost()
        {
            // var controller = new EventController(new MemoryEventRepository());
            var controller = new EventController(new NHEventRepository(SessionFactory.OpenSession()));
            var ret = controller.Post(
                new CalendarEventDetail {
                    DateTimeLocalFromGE = new DateTime(2020, 1, 1),
                    DateTimeLocalToL = new DateTime(2020, 1, 10),
                    Event = new CalendarEvent {
                        Type = new KT_CalendarEventType_Internal {
                            Id = 1
                        },
                        CountrySubdivision = new KT_CountrySubdivision_Internal {
                            Id = 1
                        }
                    }
                });

            var action = Assert.IsType<ActionResult<CalendarEventDetail>>(ret);
            var result = Assert.IsType<CreatedAtActionResult>(action.Result);
            var value = Assert.IsType<CalendarEventDetail>(result.Value);

            Assert.Equal(1, value.Id);
            Assert.Equal("Winterferien", value.Event.Type.Value);
        }

        [Fact]
        public void EventControllerPut()
        {
            var ev = new CalendarEventDetail {
                DateTimeLocalFromGE = new DateTime(2019, 1, 1),
                DateTimeLocalToL = new DateTime(2019, 1, 10),
                Event = new CalendarEvent {
                    Type = new KT_CalendarEventType_Internal {
                        Id = 1
                    },
                    CountrySubdivision = new KT_CountrySubdivision_Internal {
                        Id = 1
                    }
                }
            };
            var controller = new EventController(new NHEventRepository(SessionFactory.OpenSession()));
            var ret = controller.Put(1, ev);

            Assert.IsType<NoContentResult>(ret);

            ret = controller.Put(99, ev);
            Assert.IsType<NotFoundResult>(ret);
        }
    }
}
